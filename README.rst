#########
Movie AKA
#########

This tiny script helps in fetching various movie information in different languages.

Currently supported sources are:

* Douban (China only);
* IMDB;

Installation
============

You will need to install Python of the newest version supported by your OS.
Then use ``pip`` to install required libraries
listed in the ``requirements.txt`` file::

    $ pip install -r requirements.txt

Usage
=====

Douban
------

To search for a Chinese name of a movie on Douban try this::

    $ ./movie_aka.py douban-title "black sails"
    $ China: 黑帆 第一季

IMDB
----

To search for AKAs on IMDB run something like this::

    $ ./movie_aka.py imdb-title --year 2014 interstellar
    $ China (Mandarin title): Xing Ji Chuan Yue
    $ China (Mandarin title) (alternative transliteration): 星际穿越

Some movies will not have a title in China, try Taiwan instead::

    $ ./movie_aka.py imdb-title --country taiwan --year 2014 "black sails"
    $ Taiwan: 黑帆

Batch mode
----------

There's a way to provide an ``.xlsx`` spreadsheet and have the missing
information filled automatically for all listed movies::

    ./movie_aka.py fill-spreadsheet movies.xlsx

Supported command options are:

* ``--start-row=<n>`` (default: ``2``),
  specifies the row number in the spreadsheet where search should begin from,
  for example::

      ./movie_aka.py fill-spreadsheet --start-row=33

* ``--country=<name>`` (default: ``china``), similar to the single fetch commands.
* ``--interactive/--non-interactive`` (default: ``non-interactive``),
  in the ``interactive`` mode will ask for confirmation after each movie,
  otherwise it will apply a random delay between fetches to avoid
  excessive frequency of requests.

Spreadsheet format
~~~~~~~~~~~~~~~~~~

Spreadsheet format requires row 1 to be dedicated for column names.
Following column names are supported as input information:

* ``english_name``, example value: ``Interstellar``;
* ``year``, example value: ``2014``.

Additionally columns named as ``{source}.link`` are supported, for example:

* ``imdb.link``, example value: ``https://www.imdb.com/title/tt0272152/``;
* ``douban.link``, example value: ``https://movie.douban.com/subject/3660130/``;

Output columns will be automatically added as relevant information gets found,
and follow this format: ``{source}.{label}``, e.g.: ``douban.actors`` or
``imdb.China (Mandarin)``.

TODO
====

* **Done.**
  Use an Excel spreadsheet to read title names in bulk, search and update
  localized names in corresponding columns.
* **Done.**
  Display / store links to movie pages on Douban / IMDB.
* Add Wikipedia as a source.
* **Done.**
  Fetch / store movie rating.
* **Done.**
  Fetch / store movie production country.
* Refactor the code into classes per source with unified interface.
