#!/usr/bin/env python
"""
Use IMDB or Douban to translate English titles to Chinese.

Run it like this to get detailed usage instructions:

./movie_aka.py --help
"""

import os
import re
import random
import logging
from time import sleep
from urllib.parse import urljoin, quote_plus

import click as c
import openpyxl
import requests
from requests.adapters import HTTPAdapter
from requests.packages.urllib3.util.retry import Retry
from bs4 import BeautifulSoup


logger = logging.getLogger(__name__)


CACHE_DIR = 'cache'

# web page link templates that remain the same across all movies
IMDB_SERVER_URL = 'https://www.imdb.com/'
IMDB_TITLE_AKA_URL = urljoin(IMDB_SERVER_URL, '/title/{}/releaseinfo')
IMDB_SEARCH_URL = 'https://v2.sg.media-imdb.com/suggestion/titles/{}/{}.json'
IMDB_MOVIE_URL = 'https://www.imdb.com/title/{}/'

DOUBAN_SERVER_URL = 'https://movie.douban.com/'
DOUBAN_SEARCH_URL = urljoin(DOUBAN_SERVER_URL, '/j/subject_suggest?q={}')
DOUBAN_MOVIE_URL = 'https://movie.douban.com/subject/{}/'

# details of web page source code structure that allow us to exact movie names
IMDB_AKA_TABLE_CLASS = 'akas-table-test-only'


class TitleNotFound(Exception):

    pass


def _get_douban_movie_translation(session, title):
    """
    Query Douban search engine and extract title out of the response.
    """
    title_sanitized = re.sub('[^a-z0-9]+', ' ', title, flags=re.I).strip()
    title_encoded = quote_plus(title_sanitized).lower()

    # HACK: disallow redirects as those lead to error pages
    suggestions_response = session.get(
        DOUBAN_SEARCH_URL.format(title_encoded), allow_redirects=False)
    suggestions = suggestions_response.json()

    for suggestion in suggestions:
        # return the first result only
        return suggestion['id'], suggestion['title']

    raise TitleNotFound()


def _get_douban_movie_details(session, url):
    page = _get_from_cache_or_request(url, session.get, kwargs={
        'allow_redirects': False,
    })
    if not page:
        raise TitleNotFound()
    # page = session.get(url).content
    soup = BeautifulSoup(page, features='html.parser')
    title = soup.find('title').text.replace('(豆瓣)', '').strip()
    logging.debug('Title: {}'.format(title))

    info_section = soup.find('div', {'id': 'info'})

    actor_section = info_section.find('span', {'class': 'actor'})
    if actor_section:
        actors = actor_section.findAll('a')
    else:
        actors = []
    logging.debug('Actors:')
    for actor in actors:
        logging.debug('    {}'.format(actor.text))

    director_span = info_section.find('span', text='导演')
    if director_span:
        directors = director_span.parent.findAll('a')
    else:
        directors = []
    logging.debug('Directors:')
    for director in directors:
        logging.debug('    {}'.format(director.text))

    production_span = info_section.find('span', text='制片国家/地区:')
    production = production_span.next_sibling.strip().split(' / ')
    logging.debug('Production:')
    for country in production:
        logging.debug('    {}'.format(country))

    genres = info_section.findAll('span', {'property': 'v:genre'})
    logging.debug('Genres:')
    for genre in genres:
        logging.debug('    {}'.format(genre.text))

    interest_section = soup.find('div', {'id': 'interest_sectl'})
    rating = interest_section.select('.rating_num').pop()
    logging.debug('Rating: {}'.format(rating.text))

    synopsis_section = soup.find('div', {'id': 'link-report'})
    if synopsis_section:
        synopsis = synopsis_section.select('.all')
        if synopsis:
            synopsis = synopsis.pop().text.strip()
        else:
            synopsis = synopsis_section.find('span', {'property': 'v:summary'}).text.strip()
    else:
        synopsis = ''

    logging.debug('Synopsis: {}'.format(synopsis))

    return {
        'title': title,
        'actors': [{
            'name': x.text,
            'url': urljoin(url, x.get('href')),
        } for x in actors],
        'directors': [{
            'name': x.text,
            'url': urljoin(url, x.get('href')),
        } for x in directors],
        'production': [c for c in production],
        'genres': [g.text for g in genres],
        'rating': rating.text,
        'synopsis': synopsis,
    }


def _get_imdb_first_suggestion(session, title, year=None):
    """
    Query IMDB server and extract movie ID out of the response.
    """
    title_slug = re.sub('[^a-z0-9]+', '_', title, flags=re.I).strip('_').lower()
    title_slug += '_{}'.format(year) if year else ''
    first_letter = title_slug[0]
    aka_json = session.get(
        IMDB_SEARCH_URL.format(first_letter, title_slug))
    aka = aka_json.json()
    if 'd' not in aka:
        logging.error('Douban: bad reply: %s', aka)
        raise TitleNotFound()
    for title in aka['d']:
        return title['id']


def _get_imdb_language_akas(session, title_id, lang):
    """
    Fetch IMDB "Also Known As" page of a given movie ID and extract titles.
    """
    aka_html = session.get(IMDB_TITLE_AKA_URL.format(title_id))
    soup = BeautifulSoup(aka_html.content, features='html.parser')
    akas_table = soup.select('.{}'.format(IMDB_AKA_TABLE_CLASS)).pop()
    akas = akas_table.findAll('tr')
    for row in akas:
        country, l10n = row.findAll('td')
        if lang.lower() in country.text.lower():
            yield (country.text, l10n.text)


@c.group()
@c.option('--verbosity', default='warn', help='debug, info, warn or error')
@c.pass_context
def cli(ctx, verbosity):
    """
    Groups all available commands of this script together
    and shares a "context" object across all of them
    """
    # both imdb and douban have protection against crawlers like this one,
    # we need to make them think we're a human by first visiting the main page.
    # session is what stores information that servers use to distinguish
    # between robots and humans between several requests
    ctx.obj['session'] = requests.Session()
    ctx.obj['session'].headers.update({
        # this header masks our robot with a regular browser description
        'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64; rv:89.0) Gecko/20100101 Firefox/89.0',
    })
    retries = Retry(total=5, backoff_factor=1)
    adapter = HTTPAdapter(max_retries=retries)
    ctx.obj['session'].mount('http://', adapter)
    ctx.obj['session'].mount('https://', adapter)

    logging.basicConfig(level=getattr(logging, verbosity.upper()))


@cli.command()
@c.argument('title')
@c.option('--year', type=int, help='Year')
@c.option('--country', default='China', help='Country of interest')
@c.pass_context
def imdb_title(ctx, title, year, country):
    """
    Fetch titles in a given country of a single movie from IMDB.
    """
    try:
        akas = _imdb_title(ctx.obj['session'], title, year, country)
        for _, _, country, title in akas:
            # XXX: hack to store IMDB links, but not empty titles
            if not title:
                continue
            c.secho('{}: {}'.format(country, title), fg='green')
    except TitleNotFound:
        logger.warning('No such title found: {}.'.format(title))


def _imdb_title(session, title, year, country, link=None):
    if not link:
        title_id = _get_imdb_first_suggestion(session, title, year)
        if not title_id:
            raise TitleNotFound()
        link = IMDB_MOVIE_URL.format(title_id)

        # XXX: hack to store IMDB links, but not empty titles
        yield title_id, link, 'link', None, {}
    else:
        title_id = link.strip('/').split('/').pop()

    for country, title in _get_imdb_language_akas(session, title_id, country):
        yield title_id, link, country, title, {}


@cli.command()
@c.argument('title')
@c.pass_context
def douban_title(ctx, title):
    """
    Fetch a Chinese title of a single movie from Douban.
    """
    try:
        akas = _douban_title(ctx.obj['session'], title, None, 'China')
        for _, _, country, title in akas:
            c.secho('{}: {}'.format(country, title), fg='green')
    except TitleNotFound:
        logger.warning('No such title found: {}.'.format(title))


def _douban_title(session, title, year=None, country=None, link=None):
    country = 'China'
    if link:
        details = _get_douban_movie_details(session, link)
        title_id = link.strip('/').split('/').pop()
        title = details['title']
    else:
        details = {}
        title_id, title = _get_douban_movie_translation(session, title)
    yield title_id, DOUBAN_MOVIE_URL.format(title_id), country, title, details


@cli.command()
@c.argument('url')
@c.pass_context
def douban_movie_info(ctx, url):
    _get_douban_movie_details(ctx.obj['session'], url)


@cli.command()
@c.argument('spreadsheet-file', default='movies.xlsx', type=c.Path(True))
@c.option('--country', default='China', help='Country of interest')
@c.option('--interactive/--non-interactive', help='Pause after each title')
@c.option('--start-row', default=2, type=int, help='Row to start from')
@c.pass_context
def fill_spreadsheet(ctx, spreadsheet_file, country, interactive, start_row):
    """
    Go through all titles in a spreadsheet file,
    and fill in translations using all available sources.
    """
    sources = [
        ('douban', _douban_title),
        # ('imdb', _imdb_title),
    ]

    workbook = openpyxl.load_workbook(spreadsheet_file)
    sheet = workbook.active

    for row in list(sheet.rows)[start_row - 1:]:
        headers = {c.value: c.column for c in list(sheet.rows)[0]}
        row_number = row[0].row

        sheet_title = sheet.cell(row=row_number, column=headers.get('english_title')).value
        sheet_year = sheet.cell(row=row_number, column=headers.get('year')).value

        if sheet_title is None and sheet_year is None:
            logging.warning('Empty row')
            continue

        params = {
            'session': ctx.obj['session'],
            'title': sheet_title,
            'year': sheet_year,
            'country': country,
        }

        # iterate through sources and search for titles with each one
        for source, func in sources:
            c.secho('{row}. With {source} search: {title} [{year}]... '.format(
                row=row_number, source=source, **params), nl=False)

            # use source link if it's already provided in the spreadsheet
            link_column = headers.get(f'{source}.link')
            if link_column:
                link_cell = sheet.cell(row=row_number, column=link_column)
                if link_cell.value:
                    if link_cell.value.startswith('=HYPERLINK'):
                        params.update(link=link_cell.value.split('"')[1])
                    else:
                        params.update(link=link_cell.value)

            try:
                results = list(func(**params))
            except TitleNotFound:
                c.secho('Not found', fg='red')
                continue

            for title_id, link, source_country, title, details in results:
                # 1. store the link
                link_column_number = _get_or_add_column(sheet, f'{source}.link')
                link_value = f'=HYPERLINK("{link}", "{title_id}")'
                link_cell = sheet.cell(
                    row=row_number, column=link_column_number,
                    value=link_value)
                link_cell.style = 'Hyperlink'

                # XXX: hack to store IMDB links, but not empty titles
                if not title:
                    continue

                # 2. store all found titles
                header_name = f'{source}.{source_country}'
                c.secho('Found: {} ({})'.format(title, source_country), fg='green')
                column_number = _get_or_add_column(sheet, header_name)
                sheet.cell(row=row_number, column=column_number, value=title)

                # 3. store additional details
                for label, value in details.items():
                    header_name = f'{source}.{label}'
                    column_number = _get_or_add_column(sheet, header_name)
                    style = 'Normal'
                    if type(value) is list and len(value) > 0:
                        if type(value[0]) is dict:
                            # value = ', '.join('=HYPERLINK("{name}", "{url}")'.format(**x) for x in value)
                            value = ', '.join(x['name'] for x in value)
                            style = 'Hyperlink'
                        else:
                            value = ', '.join(x for x in value)
                    else:
                        value = str(value)

                    sheet.cell(row=row_number, column=column_number, value=value)
                    link_cell.style = style


        # save every row to not lose any data if process crashes
        workbook.save(spreadsheet_file)

        if interactive:
            c.secho('Continue? [Y/n] ', nl=False)
            if c.getchar() == 'n':
                break
        else:
            # make sure we don't send requests too frequently
            sleep(random.randint(1, 10))


def _get_or_add_column(sheet, header_name):
    headers = [h for h in list(sheet.rows)[0] if h.value]
    column_number = len(headers) + 1
    for header in headers:
        if header.value.lower() == header_name.lower():
            column_number = header.column
            break

    if column_number > len(headers):
        c.secho('Add column: {}'.format(header_name), fg='magenta')
        sheet.cell(row=1, column=column_number, value=header_name)

    return column_number


def _get_from_cache_or_request(url, method, args=[], kwargs={}):
    cache_name = '{}.html'.format(re.sub('[^a-z0-9]+', '_', url, flags=re.I))
    cache = os.path.join(CACHE_DIR, cache_name)

    if not os.path.exists(CACHE_DIR):
        os.makedirs(CACHE_DIR)
    if os.path.exists(cache):
        return open(cache).read()

    response = method(url, *args, **kwargs)
    if response.status_code != 200:
        return None
    open(cache, 'w').write(response.text)
    return response.content


if __name__ == '__main__':
    cli(obj={})
